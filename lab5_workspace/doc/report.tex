\documentclass[fleqn,numbers=noenddot,headinclude,%twoside,%1headlines,%
				11pt,a4paper,footinclude,%
				cleardoublepage=empty,abstractoff %
                ]{scrartcl}

\usepackage[hyphens]{url}
\usepackage{float}

\def\tomas{Tom\'{a}\v{s} Peterka}
\def\tomasid{tompe625}
\def\philipp{Philipp Klippel}
\def\philippid{phikl641}

\def\laboratorynr{5}
\def\laboratoryname{Reinforcement learning}
\def\date{\today}


\input{preambl}

%\usepackage[subfigure]{tocloft}
%\renewcommand{\cftpartleader}{\cftdotfill{\cftdotsep}} % for parts
%\renewcommand{\cftchapleader}{\cftdotfill{\cftdotsep}} % for chapters
%\renewcommand{\cftsecleader}{\cftdotfill{\cftdotsep}} % for sections

\usepackage[sl,SF]{subfigure}
\usepackage{wrapfig}
\usepackage[hang,marginal]{footmisc}
\usepackage[justification=raggedright, margin=1cm,singlelinecheck=false,format=hang, labelfont={sl,small,rm},font={small,sf}, skip=10pt]{caption}
\renewcommand*\thesubfigure{\alph{subfigure}}

\DeclareMathOperator*{\argmax}{arg\,max}
\DeclareMathOperator*{\abs}{abs}

\makeatletter
\newcommand\wrapfill{\par
    \ifx\parshape\WF@fudgeparshape
    \nobreak
    \vskip-\baselineskip
    \vskip\c@WF@wrappedlines\baselineskip
    \allowbreak
    \WFclear
    \fi
}
\makeatother

\setlength{\intextsep}{6pt}

\usepackage{titlesec}
\titleformat{\section}{\large\bfseries}{\thesection}{1em}{}
\titleformat{\subsection}{\normalsize\bfseries}{\thesection}{1em}{}
\titleformat{\subsubsection}{\normalsize\bfseries}{\thesection}{1em}{}

\usepackage{amsmath}
\usepackage{graphicx}

\begin{document}

\makesavenoteenv[figure*]{figure}
\pagestyle{empty}
\onecolumn
%%Manuelle titelseite
\null  % Empty line
\nointerlineskip  % No skip for prev line
\vspace*{-1cm}
\begin{center}

\includegraphics[width=7cm,keepaspectratio=true]{./imgs/logo_hori.pdf}\\[0.3cm]
Department of Computer and Information Science

\vspace{1.2cm}

\Large TDDC17 -- Artificial Intelligence\\[0.5cm]
Laboratory \laboratorynr \\ \laboratoryname \normalsize\\

\vspace{0.7cm}

\begin{tabular}{cc}
\tomas & \philipp \\
\tomasid @student.liu.se & \philippid @student.liu.se\\
\end{tabular} \\[0.7cm]
\date
\end{center}
\vfill
\begin{abstract}
The assignment was to use the reinforcement learning method \emph{Q-learning} to develop an autonomous space shuttle. Two problems had to be solved -- to keep the shuttle straight north and to make the shuttle stand still. We used sophisticated reward functions and then realized that it is more dependent on the representation of states.
\end{abstract}
\vfill
\tableofcontents

\clearpage

\pagestyle{scrheadings}
\setcounter{page}{1}

\section{Implementation of QLearning Controller}

We changed the code to be more flexible. We are using the strategy pattern for the StateAndReward assignment. That enables us to change StateAndReward on the fly with key 2. Key 1 is bounded toggling log printing on and off.

StateAndReward is now an abstract class with some helper methods. We implemented a precise function, a linear function and a normal distribution function. All the functions are normalized and thus return a value in the interval $[0,1]$. Every function has its maximum at the specified point.
\\
\lstset{style=java}
\begin{lstlisting}
protected double linear(double x, double max, double midpoint) {
	double xn = Math.abs(x-midpoint);
	return (max-xn) / max;
}
	
protected double norm(double x, double sigma, double mju) {
	double ee = ((x-mju)/sigma);
	return Math.exp((-1)/2.0*ee*ee); 
}

protected double precise(double x, double middle) {
		if(x < (middle + EPS) && x > (middle - EPS)) return 1.0;
		return 0.0;
	}
\end{lstlisting}


During the implementation of the Q-update method we first used the formula from the lecture.
$$ Q(s,a) \leftarrow  Q(s,a) + \alpha(R(s) + \gamma \argmax_{a' \in A} Q(s', a') - Q(s,a) ) $$
But this method does not converge to the maximal reward value. Then we added a normalization factor $(1-\alpha)$ so that the formula looks as follows
$$ Q(s,a) \leftarrow (1-\alpha)Q(s,a) + \alpha(R(s) + \gamma \argmax_{a' \in A} Q(s', a') - Q(s,a) ) $$
This method converges to a Qvalue which is not greater than the maximal reward. After careful reading we understood that the normalization factor $(1-\alpha)$ does not make big difference. Both formulas lead to convergence but the first converges faster and in the beginning it relies more on older values. The other formula keeps the value between minimal and maximal reward values. It converges slowly because in the beginning it throws out most of the old values and replaces it with the new ones. In the final implementation we left out the $(1-\alpha)$ because we got better results without it. The implementation of Q-update looks as follows\\
\clearpage
\lstset{style=java}
\begin{lstlisting}
// In Q-update we won't use model, just actions and rewards
double Qval = Qtable.get(prev_stateaction);
double Qmax = this.getMaxActionQValue(new_state);
double alpha = this.alpha(Ntable.get(prev_stateaction));
this.Qtable.put(
	prev_stateaction,
	Qval + alpha*(previous_reward + GAMMA_DISCOUNT_FACTOR*Qmax - Qval)
); 
\end{lstlisting}

\section{Task II -- Q-Learning Angle Controller}
\subsection*{State }
\begin{itemize}
	\item The \emph{angle} is formatted as "A:\%d;" in the state string. The precision of discretization of the angle is controlled via AngleStateAndReward.ANGLE\_PRECISION and default is 30.
	\item The \emph{angle difference} is formatted as "AA:\%d;" in the state string. It is computed from the previous angle as follows 
$$(\abs({previous\_angle})-\abs({dicretized\_angle})) / ANGLE\_ACCEL\_BLUR $$ The constant ANGLE\_ACCEL\_BLUR shrinks the state space. Default value for this constant is 4.
\end{itemize}
The default size of the state space is then $30*(30/4)=225$.

\subsubsection*{Reward}
\subsubsection*{angle reward}
We decided to use the linear function as a reward because then the agent in every angle has some necessity to turn to the right direction. If we used e.g. normal distribution function there would be states where turning does not increase the reward.

\subsubsection*{rotation reward}
We used this reward for the steady state. It should have dealt with the problem that the agent oscillated around the desired state. It was designed to decrease the reward only in case of fast rotation. That was a good idea but it turned out that the agent rather does not rotate when heading south and therefore it got stuck in a local minimum. We abandoned this reward because the reward should be only for the goal, not for the path.

\begin{figure}[ht]
	\centering
	\subfigure[Reward function for angle]{
		\includegraphics[width=7cm]{plots/angle.pdf}
		\label{img:angle}}%
	\hfill
	\subfigure[Reward function for rotation]{
		\includegraphics[width=7cm]{plots/accel.pdf}
		\label{img:rotation}}
	\caption{Reward function}
	\label{fig:angle_rewards}
\end{figure}

\section{Task III - Full hover controller}
The basic fact about the hover state and reward is that the providing class is a child of \verb|AngleStateAndReward| therefore all mentioned features below are used together with the angle state and rewards.

\subsection*{State}
The state for the hover controller consists from X and Y velocities. After few development iterations we found out that the best way is to be more precise around desired state. So then states are generated with the helper function center.
\lstset{style=java}
\begin{lstlisting}
protected String center(double min, double mm, double ideal, double pm, double max, double val) {
		if(val <= min || val >= max) {
			return String.format("%d", val < min ? (int)min : (int)max);
		}
		if(val <= mm || val >= pm) {
			return String.format("%d", (int)val);
		}
		return String.format("%.1f", val);
	}
// ....
xstate = String.format("X:%s;", this.center(-3, -1, 0, 1, 3, vx));
ystate = String.format("Y:%s;", this.center(-3, -1, 0, 1, 3, vy));
\end{lstlisting}
The class HoverStateAndReward resets the variables ANGLE\_ACCEL\_BLUR to 1 and ANGLE\_PRECISION to 12. Actually possible values for rotation are $[-3, 3]$. Each velocity is in the interval $[-12, 12]$ and from the definition of the state we get $1+3+10+10+3+1=28$ possible values for every velocity. To sum this up we ended up with $28^2*12*6=56448$ states. Values in Qtable are usable after 20 iterations of each state thus we need about 1.2M iterations. However we observed that states where the spacecraft is heading downwards are explored more often than states where it heads up. This may be due to the fact, that the upward direction is far less stable then heading down. 

\subsection*{Reward}
The reward function of HoverStateAndReward is composed from 3 values: angle reward, X velocity reward and Y velocity reward.

The final reward is computed as a \emph{multiplication} of all values. First we considered the basic arithmetic mean but it seemed to produce local extrema. It is possible that although some values are near zero, one reward is quite good. That results in a not really bad over all reward, although it is a really undesirable state. For example when the space ship heads straight downwards, it gets of course a pretty bad reward for the angle and the Y-velocity, but as it does not move sidewards the reward for the X-velocity it quite high. The reward function might produce some kind of local maximum or at least some not so undesirable states in special states at the edges (see Figure~\ref{fig:addvel}). Thus we came up with multiplication which only produces a big reward if all values are high.
\vspace{3ex}
\lstset{style=java}
\begin{lstlisting}
/* Reward function for the full hover controller */
public double getReward(double angle, double vx, double vy) {
		double angle_reward = super.getReward(angle, vx, vy);
		double xreward = this.norm(vx, 1, 0.0);
		double yreward = this.norm(vy, 1, 0.0);
		return angle_reward * xreward * yreward;
	}
\end{lstlisting}

\vspace*{1ex}
\begin{figure}[H]
	\centering
	\subfigure[Reward function of X velocity]{
		\includegraphics[width=7cm]{plots/vx.pdf}
		\label{fig:rx}}%
	\hfill
	\subfigure[Reward function of Y velocity]{
		\includegraphics[width=7cm]{plots/vy.pdf}
		\label{fig:ry}}
	\caption{Reward function}
	\label{fig:hover_rewards}
\end{figure}
%\vspace*{-3ex}
\begin{figure}[H]
	\centering
	\subfigure[addition of X- and Y- velocity]{
		\includegraphics[width=7cm, clip=true, trim=3cm 3cm 3cm 1.5cm]{plots/velocity.pdf}
		\label{fig:addvel}}%
	\hfill
	\subfigure[multiplication of X- and Y- velocity]{
		\includegraphics[width=7cm, clip=true, trim=3cm 3cm 3cm 1.5cm]{plots/vmulti.pdf}
		\label{fig:multvel}}
	\caption{Different ways to combine reward functions (normalized)}
	\label{fig:hover_rewards}
\end{figure}

\section{Conclusion}
The maximal amount of states is about 50 000. What is really important, is to discretize the state values heterogeneously. The discretization has to make smaller intervals as closer it gets to the desired state value.

What is surprising, is that success is not that much dependent on the reward function. Of course it has to be symmetric to $y=0$ with its maximum at $y=0$ and monotonous on each side for $y<0$ and $y>0$. To preserve these characteristics for the over all reward function one has to choose the method for combining the functions carefully (e.g. addition vs. multiplication, see Figure~\ref{fig:addvel} \& \ref{fig:multvel}).

We also got comparable results using the linear function and the precise function as a reward. The precise function took the Qvalues longer to converge but then it was more precise.
\end{document}
