import java.lang.Math;

public abstract class StateAndReward {
	public abstract String getState  (double angle, double vx, double vy);
	public abstract double getReward (double angle, double vx, double vy);
	
	protected double EPS = 1e-3;

	// ///////////////////////////////////////////////////////////
	// discretize() performs a uniform discretization of the
	// value parameter.
	// It returns an integer between 0 and nrValues-1.
	// The min and max parameters are used to specify the interval
	// for the discretization.
	// If the value is lower than min, 0 is returned
	// If the value is higher than min, nrValues-1 is returned
	// otherwise a value between 1 and nrValues-2 is returned.
	//
	// Use discretize2() if you want a discretization method that does
	// not handle values lower than min and higher than max.
	// ///////////////////////////////////////////////////////////
	public int discretize(double value, int nrValues, double min,
			double max) {
		if (nrValues < 2) {
			return 0;
		}

		double diff = max - min;

		if (value < min) {
			return 0;
		}
		if (value > max) {
			return nrValues - 1;
		}

		double tempValue = value - min;
		double ratio = tempValue / diff;

		return (int) (ratio * (nrValues - 2)) + 1;
	}

	// ///////////////////////////////////////////////////////////
	// discretize2() performs a uniform discretization of the
	// value parameter.
	// It returns an integer between 0 and nrValues-1.
	// The min and max parameters are used to specify the interval
	// for the discretization.
	// If the value is lower than min, 0 is returned
	// If the value is higher than min, nrValues-1 is returned
	// otherwise a value between 0 and nrValues-1 is returned.
	// ///////////////////////////////////////////////////////////
	public int discretize2(double value, int nrValues, double min,
			double max) {
		double diff = max - min;

		if (value < min) {
			return 0;
		}
		if (value > max) {
			return nrValues - 1;
		}

		double tempValue = value - min;
		double ratio = tempValue / diff;

		return (int) (ratio * nrValues);
	}
	
	protected double linear(double x, double max, double midpoint) {
		double xn = Math.abs(x-midpoint);
		return (max-xn) / max;
	}
	
	protected double norm(double x, double sigma, double mju) {
		double ee = ((x-mju)/sigma);
		return Math.exp((-1)/2.0*ee*ee); 
	}
	
	protected double precise(double x, double middle) {
		if(x < (middle + EPS) && x > (middle - EPS)) {
			return 1.0;
		}
		return 0.0;
	}
	
	protected String center(double min, double mm, double ideal, double pm, double max, double val) {
		if(val <= min || val >= max) {
			return String.format("%d", val < min ? (int)min : (int)max);
		}
		if(val <= mm || val >= pm) {
			return String.format("%d", (int)val);
		}
		return String.format("%.1f", val);
	}
}

class AngleStateAndReward extends StateAndReward {
	
	protected int ANGLE_PRECISION = 30;
	protected int ANGLE_ACCEL_BLUR = 4;

	private int previous_angle = ANGLE_PRECISION / 2; // little cheating, but we desperately need to know the acceleration
	
	private int accel(int iAngle) {
		return (Math.abs(this.previous_angle)-Math.abs(iAngle)) / ANGLE_ACCEL_BLUR;
	}
	
	/* State discretization function for the angle controller */
	public String getState(double angle, double vx, double vy) {
		/* I personally think that as a state there is enough 
		 * just the angle in this case.
		 * Angle UP=0.0, RIGHT=+PI/2, LEFT= -PI/2, DOWN=PI
		 * Lets use PI/20 as a EPS
		 */
		int iAngle = discretize(angle, this.ANGLE_PRECISION, -Math.PI, +Math.PI);
		int accel = this.accel(iAngle);
		String state = String.format("A:%d;AA:%d;", iAngle, accel);
		this.previous_angle = iAngle;
		return state;
	}

	/* Reward function for the angle controller */
	public double getReward(double angle, double vx, double vy) {
		/* Reward takes into account only angle. Desired state is when
		 * angle is 0.
		 */
		return this.norm(angle, 1, 0.0);
	}
}

class HoverStateAndReward extends AngleStateAndReward {
	
	public HoverStateAndReward() {
		super();
		ANGLE_PRECISION = 12;
		ANGLE_ACCEL_BLUR = 1;
	}

	/* State discretization function for the full hover controller */
	public String getState(double angle, double vx, double vy) {
		String angle_state= super.getState(angle, vx, vy);
		String xstate, ystate;
		xstate = String.format("X:%s;", this.center(-3, -1, 0, 1, 3, vx));
		ystate = String.format("Y:%s;", this.center(-3, -1, 0, 1, 3, vy));
		return xstate + ystate + angle_state;
	}

	/* Reward function for the full hover controller */
	public double getReward(double angle, double vx, double vy) {
		double angle_reward = super.getReward(angle, vx, vy);
		double xreward = this.norm(vx, 1, 0.0);
		double yreward = this.norm(vy, 1, 0.0);
//		double vreward = (xreward + yreward)/2;
		return angle_reward * xreward * yreward;
	}

}
