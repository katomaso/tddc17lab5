import static org.junit.Assert.*;

import org.junit.Test;


public class TestReward {

	@Test
	public void testDiscretize() {
		fail("Not yet implemented");
	}

	@Test
	public void testDiscretize2() {
		fail("Not yet implemented");
	}

	@Test
	public void testLinear() {
		fail("Not yet implemented");
	}

	@Test
	public void testNorm() {
		AngleStateAndReward s = new AngleStateAndReward();
		
		double sigma = 5, step = 0.1;
		double min = -12, max = 12, middle = 0;
		double res, res0 = -1.0;
		for(double d = min; d < middle; d += step) {
			res = s.norm(d, sigma, middle);
//			System.out.println(String.format("%.4f\t%.4f", d, res));
			assertTrue(res0 <= res);
			assertTrue(res < +1.001);
			assertTrue(res > -0.001);
			res0 = res;
		}
		res0 = 2.0;
		for(double d = middle; d < max; d += step) {
			res = s.norm(d, sigma, middle);
//			System.out.println(String.format("%.4f\t%.4f", d, res));
			assertTrue(res0 >= res);
			assertTrue(res < +1.001);
			assertTrue(res > -0.001);
			res0 = res;
		}
	}
	
	@Test
	public void testNorm1() {
		AngleStateAndReward s = new AngleStateAndReward();
		
		double sigma = 4, step = 0.2;
		double min = 0, max = 20, middle = 10;
		double res, res0 = -1.0;
		for(double d = min; d < middle; d += step) {
			res = s.norm(d, sigma, middle);
			//System.out.println(String.format("%.4f\t%.4f", d, res));
			assertTrue(res0 <= res);
			assertTrue(res < +1.001);
			assertTrue(res > -0.001);
			res0 = res;
		}
		res0 = 2.0;
		for(double d = middle; d < max; d += step) {
			res = s.norm(d, sigma, middle);
			//System.out.println(String.format("%.4f\t%.4f", d, res));
			assertTrue(res0 >= res);
			assertTrue(res < +1.001);
			assertTrue(res > -0.001);
			res0 = res;
		}
	}

	@Test
	public void testAngle() {
		StateAndReward sar = new AngleStateAndReward();
		double middle = 0.0, rew, rew0=0.0;
		for(double d = (-1)*Math.PI-1; d < middle; d += 0.1) {
			rew = sar.getReward(d, 0.0, 0.0);
			System.out.println(String.format("%.4f\t%.4f", d, rew));
			assertTrue(rew >= rew0);
			rew0 = rew;
		}
		rew0 = 1;
		for(double d = middle; d < Math.PI+1; d += 0.1) {
			rew = sar.getReward(d, 0.0, 0.0);
			System.out.println(String.format("%.4f\t%.4f", d, rew));
			assertTrue(rew <= rew0);
			rew0 = rew;
		}
	}
}
